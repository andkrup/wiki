#!/usr/bin/env bash
#
# Restarts the apache2 and the php-fpm service
# (Shared folders are made available *after* the services start, and the Document-root is also a shared folder)
#
vagrant ssh -c "sudo service apache2 restart; sudo service php7.0-fpm restart; ps -ef | grep 'fpm\|apache2'"

# Verify that data  directories are symlinked
targetDataDir="/var/www/wiki.vvb/dev/pages"
dataDir="/var/tmp/wiki/pages"
if [[ -L $(dataDir) && -d $(dataDir) ]];then
	echo "data symlink $dataDir OK"
else
	echo "restoring data symlink $dataDir"
	sudo ln -fsn $(targetDataDir) $(dataDir)
fi
