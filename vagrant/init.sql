grant all privileges on *.* to 'vagrant'@'%' with grant option;
flush privileges;
create database if not exists AndkrupDk default character set utf8 default collate utf8_general_ci;
grant all privileges on AndkrupDk.* to 'vagrant'@'localhost';
set password for 'vagrant'@'localhost' = password('vagrant');
