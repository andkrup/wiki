#!/usr/bin/env bash
#

update-alternatives --set editor /usr/bin/vim.basic

apt-get update

# ensure that this vagrant is allocated at least 512 Mb total memory
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'

# fetch nodejs sources
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

#apt-get install -y curl vim wget git ntp software-properties-common python-software-properties gettext nodejs zip
#apt-get install -y apache2 libapache2-mod-fastcgi

# mongo (https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

add-apt-repository -y ppa:ondrej/php

apt-get update

apt-get install -y apache2
apt-get install -y php7.4 php7.4-psr php7.4-phalcon php7.4-cli php7.4-curl php7.4-gd php7.4-intl php7.4-mbstring php7.4-xml php7.4-xsl php7.4-zip php7.4-fpm php7.4-redis php7.4-xdebug
apt-get install -y php7.4-mysql
apt-get install -y php7.4-sqlite
apt-get install -y php7.4-mongodb
apt-get install -y php-msgpack php-gettext

apt-get install -y redis-server
apt-get install -y mysql-server
apt-get install -y mongodb-org
apt-get install -y gettext nodejs zip

apt-get upgrade -y

# phpunit
wget https://phar.phpunit.de/phpunit.phar -O /usr/local/bin/phpunit
chown root:root /usr/local/bin/phpunit
chmod +x /usr/local/bin/phpunit

# download & install composer
curl -sS http://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
chown root:root /usr/local/bin/composer
chmod +x /usr/local/bin/composer

mkdir /home/vagrant/.npm-global
chown vagrant:vagrant /home/vagrant/.npm-global
npm config set prefix '/home/vagrant/.npm-global'
echo "export PATH=/home/vagrant/.npm-global/bin:\$PATH" >> /home/vagrant/.profile

# TODO: install npm executables as vagrant (as someone who has the npm-prefix in their path variable, IE. the shell needs
#  to have sourced /home/vagrant/.profile)
#  Maybe this can help: https://askubuntu.com/questions/1033217/how-to-export-env-variables-as-another-user
# For now we can test that root can execute a script line as vagrant
echo "Test run as vagrant: (echo vagrant users PATH)"
su - vagrant -c "echo \$PATH"
#su - vagrant -c "npm install -g stylus webpack"

# add ServerName directive to apache conf in order to have a valid hostname-based apache config
sed -i.bak '/^IncludeOptional sites-enabled\/\*.conf/a \\nServername localhost' /etc/apache2/apache2.conf

a2enmod actions alias proxy_fcgi rewrite

systemctl enable mongod

service mongod restart
service php7.4-fpm restart
service apache2 restart

unlink /etc/apache2/sites-enabled/000-default.conf
ln -fs /vagrant/default.conf /etc/apache2/sites-enabled/wiki.vvb.conf
ln -fs /vagrant/php7.4-fpm.ini /etc/php/7.4/fpm/php.ini
ln -fs /vagrant/php7.4-cli.ini /etc/php/7.4/cli/php.ini

cp -r /var/www/wiki.vvb/resources/data/wikipages /srv/
chown -R www-data:www-data /srv/wikipages
#chmod -R ug+ws /srv/wikipages
find /srv/wikipages -type d -exec chmod ug+ws {} +