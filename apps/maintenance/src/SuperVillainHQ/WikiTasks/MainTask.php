<?php

namespace SuperVillainHQ\WikiTasks {

	use Phalcon\Cli\Task as CliTask;

	/**
	 * Class MainTask
	 * @package SuperVillainHQ\WikiTasks
	 */
	class MainTask extends CliTask{

		function updateAction(array $args = []){}
		function checkForUpdateAction(array $args = []){}
		function resetAction(array $args = []){}
		function flushAction(array $args = []){}
	}
}


