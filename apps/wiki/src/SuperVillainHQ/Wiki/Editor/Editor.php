<?php
/**
 * Editor.php.
 *
 * TODO: Documentation required!
 */

namespace SuperVillainHQ\Wiki\Editor {

	use SuperVillainHQ\Wiki\Version;

	/**
	 * Interface Editor
	 * @package SuperVillainHQ\Wiki\Editor
	 */
	interface Editor{
		public function finalizeVersion():Version;
	}
}


