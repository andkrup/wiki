<?php

namespace SuperVillainHQ\Wiki {

	use SuperVillainHQ\Wiki\Editor\Editor;
	use SuperVillainHQ\Wiki\User\User;

	/**
	 * Class Session
	 * @package SuperVillainHQ\Wiki
	 */
	class Session{
		private $version;
		/**
		 * @var User
		 */
		private $author;
		/**
		 * @var \DateTime
		 */
		private $startDate;
		/**
		 * @var \DateTime
		 */
		private $endDate;
		/**
		 * @var Editor
		 */
		private $editor;

		function __construct(User $author, Editor $editor){
			$this->author = $author;
			$this->editor = $editor;
			$this->startDate = new \DateTime('now', new \DateTimeZone('UTC'));
		}

		function version():Version{
			return $this->version;
		}
		function author():User{
			return $this->author;
		}
		function startedAt():\DateTime{
			return $this->startDate;
		}
		function endedAt():\DateTime{
			return $this->endDate;
		}

		function end(){
			$this->version = $this->editor->finalizeVersion();
			$this->endDate = new \DateTime('now', new \DateTimeZone('UTC'));
		}

		public function save(){
		}
	}
}


