<?php

namespace SuperVillainHQ\Wiki {

	use Phalcon\Di\FactoryDefault;
	use Phalcon\Events\Event;
	use SuperVillainHQ\Config\Config;
	use SuperVillainHQ\Core\DependencyLoader;
	use Phalcon\Loader;

	/**
	 * Class WikiApplication
	 * @package SuperVillainHQ\Wiki
	 */
	class WikiApplication{
		private static $instance;
		private $di;
		private $internal;


		static function instance():WikiApplication{
			return self::$instance;
		}


		function __construct(string $configFilePath){
			Config::set($configFilePath);
			self::$instance = $this;
		}

		public function run(){
			$this->di = new FactoryDefault();
			$config = Config::instance();

			// load services
			$dependencyLoader = new DependencyLoader($this->di);
			$dependencyLoader->loadFromConfig($config, $this);

			$path = $this->absPath('apps/wiki/src/SuperVillainHQ/Wiki/Controller/');
			$loader = new Loader();
			$loader->registerDirs([$path]);
			$loader->register();

			$this->internal = new \Phalcon\Mvc\Application($this->di);

			$eventsManager = $this->di->getShared('eventsManager');

			$handlers = [];
			if ($eventHandlers = Config::instance()->application->eventHandlers) {
				foreach ($eventHandlers as $handler) {
					if($class = trim($handler->classname)){
						array_push($handlers, new \ReflectionClass($class));
					}
				}
			}
			$eventsManager->attach('application',
				function (Event $event, $app) use ($handlers){
					$method = $event->getType();

					foreach ($handlers as $reflector) {
						if ($reflector->hasMethod($method)) {
							$eventClass = $reflector->newInstance();
							$eventClass->$method($event, $app);
						}
					}
				}
			);
			$this->internal->setEventsManager($eventsManager);


			try{
				$response = $this->internal->handle(trim($_SERVER['REQUEST_URI']));
				$response->send();
			}
			catch(\Exception $e){
				echo $e->getMessage();
				var_dump($e);
			}
			exit;
		}

		function absPath(string $path = ''):string{
			if(strpos($path, '/') === 0){
				return realpath($path);
			}
			$path = rtrim($path, '\\/');
			$config = Config::instance();
			$configDir = Config::configDir();
			$basePath = trim($config->application->basePath);
			if($basePath = realpath("{$configDir}/{$basePath}")){
				$path = "{$basePath}/{$path}";
				$rPath = realpath($path);
				return $rPath;
			}
			throw new \Exception("Invalid path");
		}
	}
}


