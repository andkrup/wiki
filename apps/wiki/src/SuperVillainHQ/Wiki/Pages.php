<?php

namespace SuperVillainHQ\Wiki {

	use SuperVillainHQ\Config\Config;
	use SuperVillainHQ\Core\FileSys\LocalFileSystemApi;

	/**
	 * Class Pages
	 * @package SuperVillainHQ\Wiki
	 */
	class Pages{

		static function load(string $path):string{
			$path = str_replace('.html', '.md', $path);
			$storagePath = trim(Config::instance()->application->storage);
			$path = WikiApplication::instance()->absPath("{$storagePath}/{$path}");
			if(!$path){
				$path = WikiApplication::instance()->absPath("{$storagePath}/404.md");
			}
			$fileSys = new LocalFileSystemApi($path);
			if($markDown = $fileSys->load()){
				return $markDown;
			}
			throw new \Exception("Unable to load content buffer");
		}

		public static function update($payload, $path){
			$subDirs = explode('/', $path);
			array_pop($subDirs);

			$storagePath = WikiApplication::instance()->absPath(trim(Config::instance()->application->storage));

			$path = str_replace('.html', '.md', $path);

			$subPath = $storagePath;
			foreach ($subDirs as $dir) {
				$subPath = "{$subPath}/{$dir}";
				if(!is_dir($subPath)){
					mkdir($subPath);
				}
			}

			if(is_writable($storagePath)){
				LocalFileSystemApi::saveBuffer($payload, "{$storagePath}/{$path}");
			}
			else{
			    throw new \Exception("Invalid file permissions");
            }
		}
	}
}


