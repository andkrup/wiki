<?php

namespace SuperVillainHQ\Wiki\SiteMap {

	use SuperVillainHQ\Config\Config;

	/**
	 * Class SiteMap
	 * @package SuperVillainHQ\Wiki\SiteMap
	 */
	class SiteMap{
		const OPTION_KEY_DISPLAY = 'display';
		const DISPLAY_DEFAULT = 'default';
		const DISPLAY_TREE = 'tree';
		const DISPLAY_GROUP = 'group';

		private static $current;
		/**
		 * @var string
		 */
		private $path;
		/**
		 * @var string|null
		 */
		private $optionDisplay;

		function __construct(string $path, \stdClass $displayOptions = null){
			$this->path = $path;
			if(!is_null($displayOptions)){
				self::parseOptions($this, $displayOptions);
			}
		}

		private static function parseOptions(SiteMap &$instance, \stdClass $options){
			if(property_exists($options, self::OPTION_KEY_DISPLAY)){
				$instance->optionDisplay = self::parseOption(self::OPTION_KEY_DISPLAY, $options->{self::OPTION_KEY_DISPLAY});
			}
		}

		public static function setCurrent(SiteMap $instance){
			self::$current = $instance;
		}

		public static function current(){
			return self::$current;
		}

		/**
		 * @param string $option
		 * @param string $value
		 * @return string|null
		 */
		public static function parseOption(string $option, string $value) {
			switch ($option){
				case self::OPTION_KEY_DISPLAY:
					if($value == self::DISPLAY_DEFAULT || $value == self::DISPLAY_TREE || $value == self::DISPLAY_GROUP){
						return $value;
					}
					break;
			}
			return null;
		}


		function render(string $format){
			switch ($this->optionDisplay) {
				case self::DISPLAY_DEFAULT:
					$render = new DefaultDisplayRender($this->path, $format);
					break;
				case self::DISPLAY_TREE:
					$render = new TreeDisplayRender($this->path, $format);
					break;
				case self::DISPLAY_GROUP:
					$render = new GroupDisplayRender($this->path, $format);
					break;
			}
			if(isset($render)){
				$render->render();
				echo $render->buffer();
			}
		}
	}
}


