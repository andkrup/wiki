<?php


namespace SuperVillainHQ\Wiki\SiteMap {


	trait Buffering {
		/**
		 * @var string
		 */
		private $buffer;

		/**
		 * @return string
		 */
		public function buffer(): string {
			return $this->buffer;
		}

		public function resetBuffer(){
			$this->buffer = '';
		}
	}
}
