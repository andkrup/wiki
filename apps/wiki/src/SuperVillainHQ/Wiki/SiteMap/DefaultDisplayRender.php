<?php


namespace SuperVillainHQ\Wiki\SiteMap {


	use SuperVillainHQ\Config\Config;

	class DefaultDisplayRender implements FilesDisplayRender{

		use Buffering;

		/**
		 * @var string
		 */
		private $path;
		/**
		 * @var string
		 */
		private $format;

		function __construct(string $path, string $format) {
			$this->path = $path;
			$this->format = $format;
		}

		public function render():bool{
			$recursDirectory = new \RecursiveDirectoryIterator($this->path);
			$storagePath = Config::instance()->application->storage;

			$recursIterator = new \RecursiveIteratorIterator($recursDirectory);
			foreach ($recursIterator as $fileInfo) {
				if($fileInfo instanceof \SplFileInfo){
					$fileName = $fileInfo->getFilename();
					if($fileName !== '.' && $fileName !== '..'){

						$fileName = str_replace('.md', '.html', $fileName);
						$path = $fileInfo->getPathInfo();
						$path = ltrim(str_replace($storagePath, '', $path), '/');
						$path = "/" . ltrim("/{$path}/{$fileName}", '/');

						$buffer = str_replace('{{PATH}}', $path, $this->format);
						$buffer = str_replace('{{NAME}}', $path, $buffer);
						$this->buffer .= $buffer;
					}
				}
			}
			if(strlen($this->buffer)){
				return true;
			}
			return false;
		}
	}
}
