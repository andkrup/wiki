<?php


namespace SuperVillainHQ\Wiki\SiteMap {


	interface FilesDisplayRender {
		public function render():bool;
		public function buffer(): string;
	}
}
