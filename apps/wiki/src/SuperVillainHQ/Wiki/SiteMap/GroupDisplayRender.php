<?php


namespace SuperVillainHQ\Wiki\SiteMap {


	use SuperVillainHQ\Config\Config;

	class GroupDisplayRender  implements FilesDisplayRender {

		use Buffering;

		/**
		 * @var string
		 */
		private $path;
		/**
		 * @var string
		 */
		private $format;

		function __construct(string $path, string $format) {
			$this->path = $path;
			$this->format = $format;
		}

		public function render():bool{
			$recursDirectory = new \RecursiveDirectoryIterator($this->path);
			$storagePath = Config::instance()->application->storage;

			$recursIterator = new \RecursiveIteratorIterator($recursDirectory);
			$groups = [];
			foreach ($recursIterator as $fileInfo) {
				if($fileInfo instanceof \SplFileInfo){
					$fileName = $fileInfo->getFilename();
					if($fileName !== '.' && $fileName !== '..'){
						$path = $fileInfo->getPathInfo();

						$fileName = str_replace('.md', '.html', $fileName);
						$path = ltrim(str_replace($storagePath, '', $path), '/');
						$path = "/" . ltrim("/{$path}/{$fileName}", '/');

						$groupId = dirname($path);
						if(!array_key_exists($groupId, $groups)){
							$base = ucfirst(str_replace('_', ' ', ltrim(basename($groupId), '/')));
							$groups[$groupId] = "<h5>{$base}:</h5>";
						}

						$buffer = str_replace('{{PATH}}', $path, $this->format);
						$buffer = str_replace('{{NAME}}', $path, $buffer);
						$groups[$groupId] .= $buffer;
					}
				}
			}
			while($buffer = array_shift($groups)){
				$this->buffer .= $buffer;
			}
			if(strlen($this->buffer)){
				return true;
			}
			return false;
		}

	}
}
