<?php

namespace SuperVillainHQ\Wiki\BreadCrumbs {

	use Phalcon\Di;
	use Phalcon\Events\Event;

	/**
	 * Class BreadCrumbs
	 * @package SuperVillainHQ\Wiki\BreadCrumbs
	 */
	class BreadCrumbs{
		private static $trail;

		static function trail(){
			return self::$trail;
		}

		function afterHandleRequest(Event $event, $application){
//			$type = $event->getType();
//			$dispatcher = Di::getDefault()->get('dispatcher');
			$request = Di::getDefault()->get('request');
			$uri = $request->getURI();

			$parts = (object) parse_url($uri);
			self::$trail = str_replace('/', '&#187;', dirname($parts->path));
		}
	}
}


