<?php
/**
 * Version.php.
 *
 * TODO: Documentation required!
 */

namespace SuperVillainHQ\Wiki {


	/**
	 * Interface Version
	 * @package SuperVillainHQ\Wiki\User
	 */
	interface Version{
		function previousVersion():Version;

		/**
		 * Content as raw data
		 * @return string
		 */
		function raw():string;

		/**
		 * Content in html-markup
		 * @return string
		 */
		function html():string;

		/**
		 * Content in editor-formatted markup
		 * @return string
		 */
		function markup():string;
	}
}


