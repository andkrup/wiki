<?php

namespace SuperVillainHQ\Wiki\Controller {

	use Michelf\Markdown;
	use Michelf\MarkdownExtra;
	use Phalcon\Di;
	use Phalcon\Mvc\Controller;
	use SuperVillainHQ\Config\Config;
	use SuperVillainHQ\Markdown\WikiMarkdownParser;
	use SuperVillainHQ\Wiki\Pages;

	/**
	 * Class Wiki
	 * @package SuperVillainHQ\Wiki\Controller
	 */
	class WikiController extends Controller{

		protected function onConstruct(){
//			echo "construct";
		}

		function loadAction(string $file = null){
			if(empty($file)){
				$file = 'index.html';
			}
			$title = trim(Config::instance()->application->title);
			$request = Di::getDefault()->get('request');
			$markdown = Pages::load($file);

			$parser = new WikiMarkdownParser();
			$html = $parser->transform($markdown);

			$anchors = WikiMarkdownParser::fragments();

			$this->view->setVar('title', $title);
			$this->view->setVar('file', $file);
			$this->view->setVar('html', $html);
			$this->view->setVar('anchors', $anchors);

			if($request->hasQuery('edit')){
				$this->view->setVar('markdown', $markdown);
			}
		}


		function updateAction(string $file = null){
			if(empty($file)){
				$file = 'index.html';
			}
			$request = Di::getDefault()->get('request');
			if($request->isPost()){
				// TODO: update file
				$payload = $request->getPost('markdown');
				Pages::update($payload, $file);
			}
			return $this->response->redirect("/{$file}");
		}


		function sitemapAction(){}
	}
}


