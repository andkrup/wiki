<?php

namespace SuperVillainHQ\Wiki\Controller {

	use Phalcon\Di;
	use Phalcon\Mvc\Controller;
	use SuperVillainHQ\Config\Config;
	use SuperVillainHQ\Wiki\SiteMap\SiteMap;

	/**
	 * Class SitemapController
	 * @package SuperVillainHQ\Wiki\Controller
	 */
	class SitemapController extends Controller{

		function indexAction(){
			$request = Di::getDefault()->get('request');
			$storagePath = Config::instance()->application->storage;
			$options = (object) [
				'display' => $request->hasQuery('display') ? SiteMap::parseOption('display', $request->getQuery('display')) : SiteMap::DISPLAY_DEFAULT
			];
			SiteMap::setCurrent(new SiteMap($storagePath, $options));
		}
	}
}


