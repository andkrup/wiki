<?php

namespace SuperVillainHQ\Wiki {

	use SuperVillainHQ\Wiki\User\User;

	/**
	 * Class Page
	 * @package SuperVillainHQ\Wiki
	 */
	class Page implements Versionable, Version{

		function load(){
			// TODO: Implement load() method.
		}

		function save(){
			// TODO: Implement save() method.
		}

		function lock(User $user){
			// TODO: Implement lock() method.
		}

		function isLocked(): bool{
			// TODO: Implement isLocked() method.
		}

		function previousVersion(): Version{
			// TODO: Implement previousVersion() method.
		}

		/**
		 * Content as raw data
		 * @return string
		 */
		function raw(): string{
			// TODO: Implement raw() method.
		}

		/**
		 * Content in html-markup
		 * @return string
		 */
		function html(): string{
			// TODO: Implement html() method.
		}

		/**
		 * Content in editor-formatted markup
		 * @return string
		 */
		function markup(): string{
			// TODO: Implement markup() method.
		}

		function versions(): array{
			// TODO: Implement versions() method.
		}

		function current(): int{
			// TODO: Implement current() method.
		}

		function currentVersion(): Version{
			// TODO: Implement currentVersion() method.
		}
	}
}


