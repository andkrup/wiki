<?php

namespace SuperVillainHQ\Wiki\Dependencies {

	use SuperVillainHQ\Core\Dependency;
	use Phalcon\Mvc\View as MvcView;
	use SuperVillainHQ\Wiki\WikiApplication;

	/**
	 * Class View
	 * @package SuperVillainHQ\Wiki\Dependencies
	 */
	class View implements Dependency{

		function name(): string{
			return 'view';
		}

		function shared(): bool{
			return true;
		}

		function definition(){
			return function(){
				$view = new MvcView();

				$path = WikiApplication::instance()->absPath('apps/wiki/views');
				$view->setViewsDir($path);

//				$view->registerEngines(['.phtml' => 'Phalcon\Mvc\View\Engine\Php']);

				return $view;
			};
		}
	}
}


