<?php

namespace SuperVillainHQ\Wiki\Dependencies {

	use SuperVillainHQ\Core\Dependency;
	use Phalcon\Mvc\Router as MvcRouter;

	/**
	 * Class Router
	 * @package SuperVillainHQ\Wiki\Dependencies
	 */
	class Router implements Dependency{

		function name(): string{
			return 'router';
		}

		function shared(): bool{
			return false;
		}

		function definition(){
			return function(){
				$router = new MvcRouter(false);

				$router->setDefaultController('wiki');
				$router->setDefaultAction('load');
				$router->removeExtraSlashes(true);

				$router->add(
					'/{path:[a-zA-Z0-9.:/-_]*}',
					[
						'controller' => 'wiki',
						'action' => 'load',
						'path' => 1,
					]
				);
//				$router->add(
//					'/:params',
//					[
//						'controller' => 'wiki',
//						'action' => 'load',
//						'params' => 1,
//					]
//				);
				$router->add(
					'/update/{path:[a-zA-Z0-9.:/-_]*}',
					[
						'controller' => 'wiki',
						'action' => 'update',
						'path' => 1,
					]
				);
//				$router->add(
//					'/update/:params',
//					[
//						'controller' => 'wiki',
//						'action' => 'update',
//						'params' => 1,
//					]
//				);
				$router->add(
					'/sitemap/:params',
					[
						'controller' => 'sitemap',
						'action' => 'index',
						'params' => 1,
					]
				);
				$router->add(
					'/media/:params',
					[
						'controller' => 'media',
						'action' => 'index',
						'params' => 1,
					]
				);
//				$router->add(
//					'/:controller',
//					[
//						'controller' => 1
//					]
//				);
//				$router->add(
//					"/:controller/:action/:params",
//					[
//						"controller" => 1,
//						"action"     => 2,
//						"params"     => 3
//					]
//				);
				return $router;
			};
		}
	}
}


