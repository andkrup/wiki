<?php

namespace SuperVillainHQ\Wiki\Dependencies {

	use SuperVillainHQ\Config\Config;
	use SuperVillainHQ\Core\Credentials\CredentialsStorage;
	use SuperVillainHQ\Core\Dependency;

	/**
	 * Class Mongo
	 * @package SuperVillainHQ\Wiki\Dependencies
	 */
	class Mongo implements Dependency{
		public function shared():bool{
			return true;
		}

		public function name():string{
			return 'mongo';
		}

		public function definition(){
			return function(){
				$mongoConfig = Config::get('mongo');
				$host = trim($mongoConfig->host);
				$port = trim($mongoConfig->port);
				$db = trim($mongoConfig->database);
				$credentials = CredentialsStorage::instance()->getCredentials('mongo');
				$connection = "mongodb://{$credentials[0]->user}:{$credentials[0]->password}@{$host}:{$port}/{$db}";
				$mongo = new \MongoDB\Client($connection);
				return $mongo->selectDatabase($db);
			};
		}
	}
}


