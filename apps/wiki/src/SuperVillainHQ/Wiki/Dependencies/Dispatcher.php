<?php

namespace SuperVillainHQ\Wiki\Dependencies {

	use Phalcon\Di;
	use SuperVillainHQ\Core\Dependency;
	use Phalcon\Mvc\Dispatcher as MvcDispatcher;

	/**
	 * Class Dispatcher
	 * @package SuperVillainHQ\Wiki\Dependencies
	 */
	class Dispatcher implements Dependency{

		function name(): string{
			return 'dispatcher';
		}

		function shared(): bool{
			return true;
		}

		function definition(){
			return function(){
				$di = Di::getDefault();
				$eventsManager = $di->getShared('eventsManager');

//				$eventsManager->attach(
//					"dispatch:beforeException",
//					function ($event, $dispatcher, $exception) use ($di) {
//						$dispatcher->setParam('error', $exception);
//
//						switch ($exception->getCode()) {
//							case MvcDispatcher::EXCEPTION_HANDLER_NOT_FOUND:
//							case MvcDispatcher::EXCEPTION_ACTION_NOT_FOUND:
//								$dispatcher->forward(
//									array(
//										'controller' => 'session',
//										'action' => 'error404',
//									)
//								);
//								return false;
//						}
//						// no need to log exception on bugsnag if the error is a wrong url
//						$bugsnag = $di->get('bugsnag');
//						$bugsnag->notifyException($exception);
//
//						$dispatcher->forward(
//							array(
//								'controller' => 'error',
//								'action' => 'index',
//							)
//						);
//						return false;
//					}
//				);
				$dispatcher = new MvcDispatcher();
				$dispatcher->setEventsManager($eventsManager);
				$dispatcher->setDefaultNamespace('SuperVillainHQ\Wiki\Controller');
				return $dispatcher;
			};
		}
	}
}


