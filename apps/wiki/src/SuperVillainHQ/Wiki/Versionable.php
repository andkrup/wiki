<?php
/**
 * Versionable.php.
 *
 * TODO: Documentation required!
 */

namespace SuperVillainHQ\Wiki {

	/**
	 * Interface Versionable
	 * @package SuperVillainHQ\Wiki
	 */
	interface Versionable extends Editable{
		function versions():array;
		function current():int;
		function currentVersion():Version;
	}
}


