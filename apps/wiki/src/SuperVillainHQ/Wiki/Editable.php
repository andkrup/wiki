<?php
/**
 * Editable.php.
 *
 * TODO: Documentation required!
 */

namespace SuperVillainHQ\Wiki {

	use SuperVillainHQ\Wiki\User\User;

	/**
	 * Interface Editable
	 * @package SuperVillainHQ\Wiki
	 */
	interface Editable{
		function load();
		function save();
		function lock(User $user);
		function isLocked():bool;
	}
}


