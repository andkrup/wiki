<?php


namespace SuperVillainHQ\Core\FileSys {


	class LocalFileSystemApi implements FileSystemApi {

		/**
		 * @var string
		 */
		private $path;

		function __construct($path) {
			if(is_string($path)){
				$this->path = $path;
			}
		}

		public static function saveBuffer(string $buffer, string $path):bool{
		    if(!is_file($path)){
		        $dir = dirname($path);
		        if(is_dir($dir) && is_writable($dir)){
		            touch($path);
                }
            }
			if(false === file_put_contents($path, $buffer)){
			    throw new \Exception("Failed to write buffer to path {$path}");
            }
			return true;
		}

		public function load() {
			if($buffer = file_get_contents($this->path)){
				return $buffer;
			}
			return null;
		}
	}
}
