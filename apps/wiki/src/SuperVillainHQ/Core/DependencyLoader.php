<?php

namespace SuperVillainHQ\Core {

	use Phalcon\Di\DiInterface;
	use SuperVillainHQ\Config\Config;
	use SuperVillainHQ\Wiki\WikiApplication;

	/**
	 * Class DependencyLoader
	 * @package SuperVillainHQ\Core
	 */
	class DependencyLoader{
		/**
		 * @var DiInterface
		 */
		private $di;

		function __construct(DiInterface $di){
			$this->di = $di;
		}

		public function loadFromConfig(Config $config, WikiApplication $app){
			if($dependencyInfo = $config->application->dependencies[0]){
				$namespace = trim($dependencyInfo->namespace);
				if($dependencyPath = $app->absPath($dependencyInfo->src)){
					$iterator = new \DirectoryIterator($dependencyPath);
					foreach ($iterator as $fileInfo) {
						if($fileInfo->isFile() && $fileInfo->getExtension() === 'php'){
							$clsName = $fileInfo->getBasename('.php');
							$clsPath = "{$namespace}\\{$clsName}";
							$reflector = new \ReflectionClass($clsPath);
							if($reflector->implementsInterface("SuperVillainHQ\\Core\\Dependency")){
								$dependency = $reflector->newInstance();
								$name = $dependency->name();
								$shared = $dependency->shared();
								$definition = $dependency->definition();

								$this->di->set($name, $definition, $shared);
							}
						}
					}
				}
			}
		}
	}
}


