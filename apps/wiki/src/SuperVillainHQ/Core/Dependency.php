<?php

namespace SuperVillainHQ\Core {

	/**
	 * Class Dependency
	 * @package SuperVillainHQ\Core
	 */
	interface Dependency{

		function name():string;
		function shared():bool;
		function definition();
	}
}


