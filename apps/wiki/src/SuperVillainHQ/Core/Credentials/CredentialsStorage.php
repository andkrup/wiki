<?php
/**
 * PhpStorm.
 *
 * TODO: Documentation required!
 */

namespace SuperVillainHQ\Core\Credentials {

	use SuperVillainHQ\Config\Config;

	class CredentialsStorage{
		private static $instance;
		private $auth;

		static function instance(){
			if(!self::$instance){
				self::$instance = new CredentialsStorage();
			}
			return self::$instance;
		}

		private function loadFromConfig($path){
			if(!file_exists($path)){
				throw new \Exception("Auth file doesn't exist");
			}
			$authData = file_get_contents($path);
			$this->auth = new \SimpleXMLElement($authData);
		}

		private function __construct(){
			$cfg = Config::get('application');
			$path = (string) $cfg->auth['path'];
			$path = Config::absPath($path);
			$this->loadFromConfig($path);
		}

		/**
		 * Fetch one or more tokens or keys for authenticating with a remote service identified by the given context.
		 *
		 * @param string $context The unique name of the service.
		 * @return array A list of tokens or keys that authenticate usage of the remote service. Any tokens in the list
		 * is assumed to allow authentication with the service.
		 */
		function getTokens(string $context){
			$tokenNode = $this->auth->xpath("//authentication/token[@context=\"{$context}\"]")[0];
			return [
				new Token((string) $tokenNode)
			];
		}

		/**
		 * Fetch a list of credentials for a given context.
		 *
		 * @param $context
		 * @return array A list of credentials that authenticate usage of the remote service. Any credentials in the list
		 * is assumed to allow authentication with the service.
		 */
		function getCredentials($context){
			$credentialsNode = $this->auth->xpath("//authentication/credentials[@context=\"{$context}\"]")[0];
			$logins = [];
			foreach ($credentialsNode->children() as $loginNodes){
				$credentials = new Credentials((string) $loginNodes['user'], (string) $loginNodes);
				array_push($logins, $credentials);
			}
			return $logins;
		}

		function getKeyPairs($context, array $keys = null):\stdClass{
			if(is_null($keys)){
				$keys['key'] = 'key';
				$keys['secret'] = 'secret';
			}
			$credentialsNode = $this->auth->xpath("//authentication/credentials[@context=\"{$context}\"]")[0];
			$key = trim($keys['key']);
			$secret = trim($keys['secret']);

			$keyPair = new \stdClass();
			foreach ($credentialsNode->children() as $node){
				$name = trim($node->getName());
				if($name == $key){
					$keyPair->{$key} = trim($node);
				}
				if($name == $secret){
					$keyPair->{$secret} = trim($node);
				}
			}
			return $keyPair;
		}
	}
}


