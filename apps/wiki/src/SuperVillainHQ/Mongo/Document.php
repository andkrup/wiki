<?php

namespace SuperVillainHQ\Mongo {


	/**
	 * Class Document
	 * @package Tasklift\Web
	 */

	use MongoDB\Collection;
	use MongoDB\Database;

	/**
	 * Class Document
	 * @package Tasklift\Mongo
	 */
	abstract class Document{

		public $_id;


		function __construct(){
		}


		protected function getCollection(){
			$mongo = $this->getDI()->get('mongo');
			if($mongo instanceof Database) {
				$collection = $mongo->selectCollection($this->getSource());

				if ($collection instanceof Collection) {
					return $collection;
				}
			}
			return null;
		}
		protected function findOne(array $parameters){
			$collection = $this->getCollection();
			return $collection->findOne($parameters);
		}

		protected static function parseBson(&$instance, \ArrayObject $data){
			if(is_null($data)){
				return;
			}
			$hashTable = $data->getArrayCopy();
			$keys = array_keys($hashTable);
			foreach ($keys as $key){
				$value = $hashTable[$key];
				if($value instanceof \ArrayObject){
					$instance->{$key} = new \stdClass();
					self::parseBson($instance->{$key}, $value);
				}
				else{
					$instance->{$key} = $value;
				}
			}
		}

		abstract public function getSource();

		/**
		 * Should update if exists, and create if not exists
		 * @return mixed
		 */
		abstract public function save();

		/**
		 * Should insert if not exists
		 * @return mixed
		 */
		abstract public function update();

		/**
		 * Should fail if already exists
		 * @return mixed
		 */
		abstract public function create();

		/**
		 * Should continue if not exists
		 * @return mixed
		 */
		abstract public function delete();
	}

}


