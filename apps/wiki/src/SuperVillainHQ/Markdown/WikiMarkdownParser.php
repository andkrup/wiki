<?php
/**
 * Created by PhpStorm.
 * User: anderskrarup
 * Date: 2019-01-18
 * Time: 09:56
 */

namespace SuperVillainHQ\Markdown {


	use Michelf\MarkdownExtra;

	class WikiMarkdownParser extends MarkdownExtra {

		private static $fragments;

		static function fragments():array{
			if(!is_array(self::$fragments)){
				self::resetFragments();
			}
			return self::$fragments;
		}

		private static function resetFragments(array $array = []){
			self::$fragments = $array;
		}

		protected function _doHeaders_callback_atx($matches) {
			$level = strlen($matches[1]);

			$defaultId = is_callable($this->header_id_func) ? call_user_func($this->header_id_func, $matches[2]) : null;
			$attr  = $this->doExtraAttributes("h$level", $dummy =& $matches[3], $defaultId);

			// insert anchor in header, if header is of certain level
			$anchor = "";
			if($level < 3){
				$fragment = trim($matches[2]);
				$anchor = "<a id=\"{$fragment}\"></a>";
				self::fragments();
				array_push(self::$fragments, $fragment);
			}


			$block = "<h$level$attr>" . $anchor . $this->runSpanGamut($matches[2]) . "</h$level>";
			return "\n" . $this->hashBlock($block) . "\n\n";
		}

	}
}
