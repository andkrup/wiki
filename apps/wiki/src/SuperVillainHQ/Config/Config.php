<?php

namespace SuperVillainHQ\Config {

	/**
	 * Class Config
	 * @package SuperVillainHQ\Config
	 */
	class Config{
		/**
		 * @var Config
		 */
		private static $instance;
		private $filePath;
		private $jsonData;

		private function __construct(string $filePath){
			$this->filePath = $filePath;

			if(is_readable($filePath)){
				$this->jsonData = json_decode(file_get_contents($filePath));
			}
		}

		static function set(string $filePath){
			self::$instance = new Config($filePath);
		}

		static function instance():Config{
			return self::$instance;
		}

		static function configPath():string{
			return self::instance()->filePath;
		}
		static function configDir():string{
			return dirname(self::configPath());
		}


		function __get($name){
			if(property_exists($this->jsonData, $name)){
				return $this->jsonData->{$name};
			}
			return null;
		}
	}
}


