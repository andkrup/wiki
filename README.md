# Phalcon Wiki App

Requires write permissions on pages directory for web-server user

```bash
    chown www-data /var/tmp/wiki/pages
    chmod u+ws /var/tmp/wiki/pages
```


## Roadmap

  * Sitemap
  * Media (image) upload
  * Use Mongo to store meta-information about text files (revisions, author, etc)

