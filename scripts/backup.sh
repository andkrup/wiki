#!/usr/bin/env bash

DATE=`date '+%Y%m%dT%H%M%S'`

scriptDir="$(dirname "$0")"
dataPath=${1:-"/var/tmp/wiki/pages"}

targetFileName="wiki.pages-${DATE}.tgz"
targetPath="/home/vagrant/$targetFileName"

#cd "$scriptDir/.."

tar -czf "$targetPath" "$dataPath/"

# Use the AWS cli bundle (requires python) in order to upload backup archives to an S3 bucket
