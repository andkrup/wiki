# Services


## Public Services

  * [API Gateway](/services/api_gateway.html)



## Internal System Services

Docupics uses the following main internal services in order to handle primary concerns:

  * Reports
  * Licenses
  * Customers

Furthermore the system relies on these services

  * HeartBeat
  * Content (SCN)

## Environments

We're currently running all services on the [V2Test environment](/environments/v2test.html).



### Reports

The Report service captures and processes Report data from clients (the Device App).

[Report service details](/services/report.html)  
[Report service endpoints](/services/report.html#endpoints)


### Licenses

The License service handles licensing, access and authorisation.

[License service details](/services/license.html)


### Customers

The Customer service manages Company assets.

[Customer service details](/services/customer.html)


### HeartBeat

The HeartBeat service is running processes and events.

[HeartBeat service details](/services/heartbeat.html)


### Content (SCN)

The Content service caches export data and media.

[Content service details](/services/scn.html)
