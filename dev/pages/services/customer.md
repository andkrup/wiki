# Customer service

### Description

The Customer service manages Customer entities and related entities.

## Command-line tools for management

### Customer Command

#### Create new Customer

Create a new Customer in the system.

Syntax:

    php bin/customers.phar customer create <email>

Expected output:

    New Customer created with ID: '<ID>'


### Contacts Command

#### Look-up email usage

Find all usages of a registered email-address. Intended to root out orphaned email-address data

Syntax:

    php bin/customers.phar contacts emailUsage ank@docupics.dk


Expected output will list each entity, including ID that has a relation to the specified email-address and the type of entity (User or Customer)

    There are 1 contact using the email:
      (user: 2)
      (customer: 4)

## API Endpoints


  * create new customer (POST /customer)
  * customer exists (POST /customer/exists)
  * customer search (GET /customer/search)
  * customer get (GET /customer)
