# DocuPics Domain

The DocuPics system defines the following Actors and Entities


### <a name="customer"></a> Customer

The Customer entity is our client that has acquired some Licenses for him to use in his Company.



### <a name="company"></a> Company

The Company is the context where a Customer has work being done.


### <a name="workspace"></a> Workspace


### <a name="license"></a> License


### <a name="user"></a> User

A Customer may have employed some Users in his Company


### <a name="team"></a> Team


### <a name="template"></a> Template


### <a name="report"></a> Report


### <a name="project"></a> Project
