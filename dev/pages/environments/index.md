# Environments

[V2Test](/environments/v2test.html) | [Dev](/environments/dev.html) | [Prod](/environments/production.html) | [Index](/environments/index.html)

An environment is a group of [Services](/services/index.html), that interact with each other and form a complete DocuPics application system.

In order to function as a complete DocuPics application system, an Environment must include the following [Services](/services/index.html):

  * License service
  * Customer service
  * Report service
  * HeartBeat service
  * Static-content service

Furthermore in order to manage the environment from the outside, these services may be required:

  * Api-Gateway service
  * Administration service