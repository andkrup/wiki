# System & Maintenance tools

[Entities](/administration/entities.html) |
[Components](/administration/components.html) |
[System & Maintenance](/administration/system.html) |
[By-case](/administration/use-cases.html)

## Maintenance and Diagnostics

### Command-line admin tool overview

These tools are packaged in the Core-library, which means that they should be available in every service installation.


#### The Tasklift Utility-tool

This tool is included in the core-library and is thus available in every project that uses the core-library.

First argument must be a path to the config-xml file. Subsequent arguments will be interpreted as arguments by the Sub-command (see the c-flag below)

    $ php vendor/bin/tasklift <path-to-config-xml-file> -c <command> [argument [...]]

##### Flags

  * v
  * c

###### v (verbose)

(optional)

Use this flag to allow the command to print out debug-information.

###### c (command)

(required)

The Sub-command that will be executed by the Tasklift executable


##### Self-test Command

Runs self-diagnosing functions, checking all relevant configuration values.

    $ ssh ubuntu@v2test.reports.tasklift.com
    $ cd /var/www/<any-tasklift-project>/current
    $ php vendor/bin/tasklift <path-to-config-xml-file> -c SelfTest


##### Signature hash Command

Creates a signature hash and time-of-request value for a given Device-ID, secret and endpoint.

    $ ssh ubuntu@v2test.reports.tasklift.com

    $ cd /var/www/<any-tasklift-project>/current

    $ php vendor/bin/signature.php <device-id> <secret> <endpoint-uri>


##### Client hash

Creates a signature hash and time-of-request value for a given Api-client-ID and access-token.

    $ ssh ubuntu@v2test.reports.tasklift.com

    $ cd /var/www/<any-tasklift-project>/current

    $ php vendor/bin/client.php <client-id> <accesstoken>
