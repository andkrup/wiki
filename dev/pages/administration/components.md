# Administration Components

This page describes the different options that can be managed in the Administration.

## DocuPics objects

These components allows you to manage the obvious objects in DocuPics

  * Report component
  * Template component
  * Team/User component
  * Task component

### Hidden/Advanced objects


  * PostProcessing component
  * 