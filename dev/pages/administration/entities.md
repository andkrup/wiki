# Entities

[Entities](/administration/entities.html) |
[Components](/administration/components.html) |
[System & Maintenance](/administration/system.html) |
[By-case](/administration/use-cases.html)


## API-Client

#### Command-line admin tool overview

###### Create new API-Client

    $ ssh ubuntu@v2test.reports.tasklift.com

    $ cd /var/www/docupics-api/current

    $ php bin/tasklift client create <name> <IP[,IP...]>

## Customer

#### Command-line admin tool overview

###### Create new Customer

## Company

#### Command-line admin tool overview

###### Create new Company

## License

#### Command-line admin tool overview

###### Create new License
