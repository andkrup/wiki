# Local Development Environment

[Index](/local_dev_env/index.html) |
[OSX-specific help](/local_dev_env/osx.html)

### Hosts file address mappings

Each vagrant-box has been configured with unique local IP-addresses and a unique SSH-tunnel port. This allows a developer to host and run multiple vagrant boxes simultaneously.

The apache config file in each project also sets up domain-based vhosts. Add these mappings to your local /etc/hosts file, in order to align your DNS-map with the vagrant boxes.


    192.168.56.102; 2222  admin.tasklift.vvb
    192.168.56.102; 2222  api.tasklift.vvb
    192.168.56.102; 2222  wiki.tasklift.vvb
    192.168.56.102; 2222  testresults.tasklift.vvb
    192.168.56.103; 2223  tasklift.vvb
    192.168.56.104; 2224  integration.tasklift.vvb
    192.168.56.105; 2225  customers.tasklift.vvb
    192.168.56.105; 2225  tests.customers.tasklift.vvb
    192.168.56.107; 2227  licenses.tasklift.vvb
    192.168.56.108; 2228  reports.tasklift.vvb
    192.168.56.109; 2229  tl-admin.tasklift.vvb
    192.168.56.110; 2230  backoffice.docupics.vvb
    192.168.56.111; 2231  events.tasklift.vvb
    192.168.56.112; 2232  heartbeat.tasklift.vvb
    192.168.56.113; 2233  api.docupics.vvb
    192.168.56.114; 2234  adminapi.docupics.vvb
    192.168.56.115; 2235  content.tasklift.vvb
    192.168.56.128; 2248  dp-demo.tasklift.vvb
    192.168.56.129; 2249  template-editor-test.vvb
