# Local Server Development Environment on OSX

[Index](/local_dev_env/index.html) |
[OSX-specific help](/local_dev_env/osx.html)

This guide is currently describing how to setup OSX Mojave

## Requirements

  * Xcode (requirement for MacPorts)
  * MacPorts (Homebrew is possibly an alternative)
  * VirtualBox
  * Vagrant

Simply follow install instructions for each tool.

### MacPorts *nix tools
  * git


## Suggested Tools

  * PhpStorm (IDE)
  * Fork (git GUI)
  * Insomnia (GUI rest-client)
  * Wireshark (net-diagnostics)
  * MCG (grind-file analyser)
  * Diffmerge (GUI diff/merge-tool)
  * Robomongo (GUI mongo-client)
  * Sequel Pro (GUI mysql-client)

### Phalcon auto-complete PhpStorm plugin

Install via PhpStorms internal plugin-browser

## OSX-specific issues

### SSH
https://apple.stackexchange.com/questions/254468/macos-sierra-doesn-t-seem-to-remember-ssh-keys-between-reboots
https://blog.elao.com/en/tech/ssh-agent-does-not-automatically-load-passphrases-on-the-osx-sierra-keychain/