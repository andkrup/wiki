# DocuPics Developer Wiki

(This is a markdown document. A cheatsheet is available [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet))

Important sections:

[API](/api/index.html)  
[Services](/services.html)  
[Administration](/administration/index.html)  


[App](/app/index.html)


[Template Data format](/template/template_dataformat.html)


### New to Docupics?

Take a look at this [page](/docupics/introduction.html)