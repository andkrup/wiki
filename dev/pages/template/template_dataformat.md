# Template Data format

[Template XSD](/template/template_xsd.html) | [Chapters XSD](/template/chapters_xsd.html)

The Template Data is formatted as XML which can be validated by the Template Xsd files.

### Structure

The Template Xml is divided into a meta child-node and a chapters child-code. The chapters child-node follows the rules, defined by the [chapters.xsd](/template/chapters_xsd.html).

### XML Example

    <?xml version="1.0" encoding="utf-8"?>
    <template
    		xmlns="http://schema.tasklift.com/template"
    		schemaLocation="template.xsd"
    		name="My Template"
    		hash="5a18142e8a2fd5cf4b5e0c58"
    		revision="3"
    >
    	<meta>
    		<map id="Image-map" modules="Sig1">
				<imagedata>
					<bitmap encoding="base64"><![CDATA[/9j/4AAQSkZJRgAB...SUtAH/2Q==]]></bitmap>
					<label><![CDATA[Emoji-safe Label]]></label>
					<hotspot module="txt1a">
						<label><![CDATA[Click me and input some text!]]></label>
						<vertice x="100" y="100" radius="0"/>
						<vertice x="120" y="100" radius="0"/>
						<vertice x="120" y="120" radius="0"/>
						<vertice x="100" y="120" radius="0"/>
					</hotspot>
				</imagedata>
			</map>
		</meta>
		<chapters locale="en_US">
			<plot id="map1">
				<bitmap encoding="base64"><![CDATA[/9j/4AAQSkZJRgAB...SUtAH/2Q==]]></bitmap>
				<text />
				<pin />
				<coord x="0.0" y="0.0"><![CDATA[Long description]]></coord>
			</plot>
			<chapter id="A" loopable="1">
				<meta>
					<name><![CDATA[Short Name]]></name>
					<description><![CDATA[Long description]]></description>
					<control>
						<step module="pho1">
							<decision option="opt1">
								<next module="dt1"/>
							</decision>
						</step>
					</control>
				</meta>
				<text id="txt1a" required="1" type="textinput">
					<label><![CDATA[Emoji-safe Label]]></label>
					<body><![CDATA[Emoji-safe Label]]></body>
				</text>
				<signature id="sig1">
					<label><![CDATA[Emoji-safe Label]]></label>
					<body><![CDATA[Emoji-safe Label]]></body>
				</signature>
				<photo id="pho1" required="1" drawable="1" localStorage="1">
					<body><![CDATA[Emoji-safe Label]]></body>
					<label><![CDATA[Emoji-safe Label]]></label>
					<image>
						<bitmap encoding="base64"><![CDATA[........................]]></bitmap>
					</image>
				</photo>
				<scan id="scn1">
					<body><![CDATA[Emoji-safe Label]]></body>
					<label><![CDATA[Emoji-safe Label]]></label>
				</scan>
				<select id="sel1">
					<label><![CDATA[Emoji-safe Label]]></label>
					<option id="opt1"><![CDATA[Emoji-safe Label]]></option>
					<option id="opt2"><![CDATA[Emoji-safe Label]]></option>
					<option id="opt3"><![CDATA[Emoji-safe Label]]></option>
				</select>
				<pin id="pin1">
					<label><![CDATA[Emoji-safe Label]]></label>
					<body><![CDATA[Emoji-safe Label]]></body>
				</pin>
				<date id="dt1">
					<label><![CDATA[Emoji-safe Label]]></label>
					<body><![CDATA[Emoji-safe Label]]></body>
				</date>
				<time id="tm1">
					<label><![CDATA[Emoji-safe Label]]></label>
					<body><![CDATA[Emoji-safe Label]]></body>
				</time>
			</chapter>
			<chapter id="B">
				<signature id="Sig1" required="0">
					<label><![CDATA[Emoji-safe Label]]></label>
					<body><![CDATA[Emoji-safe Body]]></body>
				</signature>
				<text id="txt1b" required="1" type="textinput">
					<label><![CDATA[Emoji-safe Label]]></label>
				</text>
				<photo id="pho1b">
					<label><![CDATA[Emoji-safe Label]]></label>
				</photo>
				<scan id="scn1b">
					<label><![CDATA[Emoji-safe Label]]></label>
				</scan>
				<select id="sel1b" min="1" max="2">
					<option id="opt4"><![CDATA[Emoji-safe Label]]></option>
					<option id="opt5"><![CDATA[Emoji-safe Label]]></option>
					<label><![CDATA[Emoji-safe Label]]></label>
				</select>
				<pin id="pin1b">
					<label><![CDATA[Emoji-safe Label]]></label>
				</pin>
				<date id="dt1b">
					<label><![CDATA[Emoji-safe Label]]></label>
				</date>
				<time id="tm1b">
					<label><![CDATA[Emoji-safe Label]]></label>
				</time>
			</chapter>
		</chapters>
	</template>