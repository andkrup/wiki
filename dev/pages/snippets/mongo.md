# Mongo Snippets


## Create user

#### Create a root user

Start by selecting the correct database:

    use admin

Create the user:

    db.createUser({user: "root",pwd: "root-password",roles: [ "root" ]})

## Dump a database

Dump a complete database into a snapshot file

(including the password in the command - password is __visible__ in process-info)

    mongodump -u <USER> -p <PASSWORD> -d <DATABASE> --authenticationDatabase=<AUTHDB> -o <PATH>

(without the password - password is hidden in process-info and will be prompted)

    mongodump -u <USER> -d <DATABASE> --authenticationDatabase=<AUTHDB> -o <PATH>

## Import data into a database

    mongorestore -u <USER> -p <PASSWORD>  --authenticationDatabase=<AUTHDB> <PATH-TO-DUMP-FILES>