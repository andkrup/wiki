# API Overview

[Public API](/api/public.html) |
[Device API](/api/device.html) |
[Admin API](/api/admin.html) |
[API Index](/api/index.html)


## Report Service endpoints

  * [/template/get](/api/public/template.html#get)
  * [/template/create](/api/public/template.html#create)

## Customer Service endpoints

#### ([Customer](/docupics/domain.html#customer))

  * [/customer/get](/api/public/customer.html#get)  
  * [/customer/create](/api/public/customer.html#create)  
  * [/customer/delete](/api/public/customer.html#delete)  

#### ([Company](/docupics/domain.html#companyr))

  * [/company/get](/api/public/company.html#get)  
  * [/company/create](/api/public/company.html#create)  
  * [/company/delete](/api/public/company.html#delete)  

#### ([User](/docupics/domain.html#user))

  * [/user/get](/api/public/user.html#get)  
  * [/user/create](/api/public/user.html#create)  
  * [/user/delete](/api/public/user.html#delete)  

#### ([Team](/docupics/domain.html#team))

  * [/team/get](/api/public/team.html#get)  
  * [/team/create](/api/public/team.html#create)  
  * [/team/delete](/api/public/team.html#delete)  


## License Service endpoints

### Internal Services

  * [Licenses](/api/public/licenses.html)
