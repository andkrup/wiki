# Public API

[Public API](/api/public.html) |
[Device API](/api/device.html) |
[Admin API](/api/admin.html) |
[API Index](/api/index.html)



## GET /ping

System-test endpoint.

This endpoint will respond with a Data-message, containing the string-value "pong" as data.


#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    GET /ping HTTP/1.1
    Host: api.docupics.com
    Api-Key: 1234
    Time-Of-Request: 23 82 80 81 90 91
    Signature: 0ae649b9af5eb115a5f0e56714ceaa9d00c59cbed672dc8ca68a7405c7855ead
    Accept: */*

## GET /status

System-test endpoint.

This endpoint will try to ping the internal services and respond with a Data-message, containing an array of messages, each describing the connectivity-status of each internal service as data.


#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    GET /status HTTP/1.1
    Host: api.docupics.com
    Api-Key: 1234
    Time-Of-Request: 23 82 80 81 90 91
    Signature: 0ae649b9af5eb115a5f0e56714ceaa9d00c59cbed672dc8ca68a7405c7855ead
    Accept: */*


## POST /session/login 

Identify yourself and receive  a fresh new Api-token for future authentication.


#### Headers

  * Content-Type

#### Example request

NB! This endpoint does not expect JSON as Content-Type

    POST /session/login HTTP/1.1
    Host: licenses.tasklift.vvb
    Content-Type: multipart/form-data
    Accept: application/json
    Content-Length: 412
    user = andkrup,
    password = 123456


#### Expected Responses

##### 200 OK

The returned object contains 1 property. The name of the property is the value of the Api-Key, and the property-value is an Access-Token.

The handle and token can be used to create Http-Headers for authenticating future requests.

    {
      "status": "success",
      "data": {
        "1234": {
          "token": "faa600dd69d8b0337f08fc1b0f13ab73",
          "userId": 0
        }
      }
    }

## GET /session/logout

Destroys the currently used Api-token, implied by the Api-Key, Time-Of-Request and Signature headers.


#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    GET /session/logout HTTP/1.1
    Host: licenses.tasklift.vvb
    Api-Key: 1234
    Time-Of-Request: 23 82 80 81 90 91
    Signature: 0ae649b9af5eb115a5f0e56714ceaa9d00c59cbed672dc8ca68a7405c7855ead
    Accept: */*

#### Expected Responses

##### 204 No Content

204 No Content is returned upon successfully deleting the access-token


## POST /client

Imports an API-Client into the local service.

The API-Client is the entity that controls Api-Keys and Access-Tokens. One API-Client can hold multiple key/token pairs.

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    POST /client?handle=my_token&identity=developer HTTP/1.1
    Host: publicapi.tasklift.vvb
    Content-Type: application/json
    Api-Key: dev-stefan
    Time-Of-Request: 12 21 21 32 32 32
    Signature: ed441b1ccedce9bced441b1ccedce9bced441b1ccedce9bced441b1ccedce9bc
    Accept: */*
    Content-Length: 412
    {
    	"id" : 1,
    	"companyId" : 1,
    	"identity" : "developer",
    	"context" : "admin",
    	"refreshToken" : "6a0ac10142a38dd3dd2f82e42709c55a039cfe1731c6410174a831aabb9eca0a"
    }



## POST /client/token

Import an Api-token.


#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Query-parameters

  * handle (required)
  * identity (optional)
  * contexts (not yet supported)

If identity is not used, the Api-Key is used as identity. This means that you are importing the access-token into the same identity that is being authenticated during the request.

The **context** parameter can be used to specify to which internal services, the Api-token will be propagated to.

#### Example request

    POST /client/token?handle=my_token&identity=developer HTTP/1.1
    Host: publicapi.tasklift.vvb
    Content-Type: application/json
    Api-Key: dev-stefan
    Time-Of-Request: 12 21 21 32 32 32
    Signature: ed441b1ccedce9bced441b1ccedce9bced441b1ccedce9bced441b1ccedce9bc
    Accept: */*
    Content-Length: 412
    {
    	"ttl" : null,
    	"token" : "076defe9342eaeb57707a3b747efef2a",
    	"allowed" : [
    		"127.0.0.1",
    		"192.168.56.1",
    		"192.168.56.113"
    	],
    	"denied" : []
    }

## DELETE /client

Removes an API-Client from the local service.


#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Query-parameters

  * identity (requiredl)

If identity is not used, the Api-Key is used as identity. This means that you are importing the access-token into the same identity that is being authenticated during the request.

#### Example request

    DELETE /client?identity=developer HTTP/1.1
    Host: publicapi.tasklift.vvb
    Accept: application/json

#### Expected Responses

##### 204 No Content

204 No Content is returned upon successfully deleting the access-token


## DELETE /client/token

Revokes an existing Api-token specified by the __handle__ query-parameter. The Client identity of which the Api-token will be revoked, is either specified with the __identity__ query-parameter, or implied by the Api-Key. See the Query-parameters section below.


#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Query-parameters

  * handle (required)
  * identity (optional)
  * contexts (not yet supported)

If identity is not used, the Api-Key is used as identity. This means that you are importing the access-token into the same identity that is being authenticated during the request.

The **context** parameter can be used to specify to which internal services, the Api-token will be propagated to.

#### Example request

    DELETE /client/token?handle=my_token&identity=developer HTTP/1.1
    Host: publicapi.tasklift.vvb
    Content-Type: multipart/form-data
    Accept: application/json
    Content-Length: 412
    user = andkrup,
    password = 123456

#### Expected Responses

##### 204 No Content

204 No Content is returned upon successfully deleting the access-token.



## GET /report/search

Fetches a list of ReportStatus objects, created by the Company related to the API-Client, having the used Api.Key.

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Query-parameters

(not yet specified)

Query-parameters will be available for passing search-filters along the request.

#### Example request

    GET /report/search HTTP/1.1
    Host: publicapi.tasklift.vvb
    Accept: application/json

#### Expected Responses

##### 200 OK

200 OK is returned with a JSON-object, containing the resulting ReportStatus objects.

    {
      "status": "success",
      "data": [
        {
          "checksum": "e3536c747b27cd16add1b1e6e7a2929f",
          "status": "processed",
          "events": {
            "capture": {
              "type": "capture",
              "date": {
                "date": "2018-12-04 14:18:13.000000",
                "timezone_type": 3,
                "timezone": "UTC"
              },
              "tz": 0
            },
            "created": {
              "type": "created",
              "date": {
                "date": "2018-12-04 14:14:00.000000",
                "timezone_type": 1,
                "timezone": "+00:00"
              },
              "tz": 0
            },
            "processed": {
              "type": "processed",
              "date": {
                "date": "2018-12-05 13:12:34.000000",
                "timezone_type": 3,
                "timezone": "UTC"
              },
              "tz": 0
            }
          },
          "revisions": 2
        },
        {
          "checksum": "e12e4f9d206e9f8787fc7d2e20069096",
          "status": "pending",
          "events": {
            "capture": {
              "type": "capture",
              "date": {
                "date": "2019-01-10 13:13:05.000000",
                "timezone_type": 3,
                "timezone": "UTC"
              },
              "tz": 0
            },
            "created": {
              "type": "created",
              "date": {
                "date": "2019-01-10 13:11:15.000000",
                "timezone_type": 1,
                "timezone": "+00:00"
              },
              "tz": 0
            }
          },
          "revisions": 1
        }
      ]
    }

## GET /template

Fetches a Template XML object, identified by the given template-ID.

#### Headers

  * Accept
  * Api-Key
  * Time-Of-Request
  * Signature

Currently, the only mimetype supported for the Accept-header, is 'application/xml'.

#### Query-parameters

  * id (required)

###### id (required)

Identifies the Template


#### Example request

    GET /template?id=5bf6b61f63623910a306af01 HTTP/1.1
    Host: publicapi.tasklift.vvb
    Accept: application/xml

#### Expected Responses

##### 200 OK

200 OK is returned with a XML-object, describing the requested Template.

    <?xml version="1.0"?>
    <template id="5bf6b61f63623910a306af01" name="All about you">
      <chapters>
        <chapter id="info" loopable="0">
          <meta>
            <name>Info</name>
          </meta>
          <text id="txt2" type="text">
            <label>Your name</label>
          </text>
        </chapter>
        <chapter id="image" loopable="0">
          <meta>
            <name>Image</name>
          </meta>
          <image id="image1" type="static">
            <label>Some beatufull flowers</label>
            <bitmap encoding="base64" width="371" height="450">
              <![CDATA[/9j/4AAQS...zRRRQB//2Q==]]>
            </bitmap>
          </image>
        </chapter>
      </chapters>
    </template>


## GET /template/search

Fetches a list of TemplateInfo objects, created by the Company related to the API-Client, having the used Api.Key.

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Query-parameters

(not yet specified)

Query-parameters will be available for passing search-filters along the request.

#### Example request

    GET /template/search HTTP/1.1
    Host: publicapi.tasklift.vvb
    Accept: application/json

#### Expected Responses

##### 200 OK

200 OK is returned with a JSON-object, containing the resulting TemplateInfo objects.

    {
      "status": "success",
      "data": {
        "templates": [
          {
            "name": "Quick",
            "token": "5bcdb37831bd5b61d40e4ee1",
            "authorId": 1,
            "chapters": 1,
            "modules": 2,
            "author": "Anders",
            "owner": "Tasklift (Anders' test)"
          },
          {
            "name": "Test-Template",
            "token": "5bf6b61f63623910a306af01",
            "authorId": 1,
            "chapters": 5,
            "modules": 16,
            "author": "Anders",
            "owner": "Tasklift (Anders' test)"
          }
        ],
        "pagingOptions": {
          "count": 2,
          "current": 1,
          "pageSize": 25,
          "overflowBehaviour": 0
        }
      }
    }




## GET /task

Fetches a Task JSON object, identified by the given task-ID.

#### Headers

  * Accept
  * Api-Key
  * Time-Of-Request
  * Signature

Currently, the only mimetype supported for the Accept-header, is 'application/json'. This header is currently not needed.

#### Query-parameters

  * task|id|taskId (required)

###### task|id|taskId (required)

The Task-identifier can be named either 'task', 'taskId' or 'id'.

#### Example request

    GET /task?id=5 HTTP/1.1
    Host: publicapi.tasklift.vvb
    Accept: application/json

#### Expected Responses

##### 200 OK

200 OK is returned with a JSON-object, describing the requested Task.

    {
      "status": "success",
      "data": {
        "id": 1,
        "legacyId": 0,
        "name": "Testopgave",
        "description": "Dette er en testopgave",
        "companyId": 1,
        "createdAt": {
          "date": "2018-10-15 13:04:46.000000",
          "timezone_type": 3,
          "timezone": "UTC"
        },
        "modifiedAt": null,
        "active": true
      }
    }



## GET /task/search

Fetches a list of Task objects, created by the Company related to the API-Client, having the used Api.Key.

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Query-parameters

(not yet specified)

Query-parameters will be available for passing search-filters along the request.

#### Example request

    GET /task/search HTTP/1.1
    Host: publicapi.tasklift.vvb
    Accept: application/json

#### Expected Responses

##### 200 OK

200 OK is returned with a JSON-object, containing the resulting Task objects and a Paging object.

    {
      "status": "success",
      "data": {
        "tasks": [
          {
            "id": 1,
            "legacyId": 0,
            "name": "Testopgave",
            "description": "Dette er en testopgave",
            "companyId": 1,
            "createdAt": {
              "date": "2018-10-15 13:04:46.000000",
              "timezone_type": 3,
              "timezone": "UTC"
            },
            "modifiedAt": null,
            "active": true
          },
          {
            "id": 714,
            "legacyId": 0,
            "name": "Test weee",
            "description": "Test",
            "companyId": 1,
            "createdAt": {
              "date": "2018-11-12 12:36:32.000000",
              "timezone_type": 3,
              "timezone": "UTC"
            },
            "modifiedAt": null,
            "active": true
          }
        ],
        "pagingOptions": {
          "count": 6,
          "current": 0,
          "pageSize": 25,
          "overflowBehaviour": 0
        }
      }
    }



## GET /team

Fetches a Team JSON object, identified by the given team-ID.

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Query-parameters

  * id (required)

###### id (required)

The id is numeric.

#### Example request

    GET /team?id=2 HTTP/1.1
    Host: publicapi.tasklift.vvb
    Accept: application/json

#### Expected Responses

##### 200 OK

200 OK is returned with a JSON-object, describing the requested Team and its related User objects.

    {
      "status": "success",
      "data": {
        "id": 24,
        "companyId": 2,
        "active": true,
        "name": "Mikkel Holst Schou  Hansen",
        "users": [
          {
            "id": 24,
            "legacyId": null,
            "firstName": null,
            "lastName": null,
            "addresses": {},
            "emails": {},
            "phones": {},
            "profile": null,
            "country": null
          }
        ]
      }
    }



## POST /team

Creates a new Team.

This endpoint only creates the Team object. Use the import endpoint to add Users in the same request as creating the Team

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Payload

    {
      "name" : "My other Team",
      "companyId" : 1
    }


#### Example request

    POST /team HTTP/1.1
    Host: publicapi.tasklift.vvb
    Content-Type: application/json
    Accept: application/json
    Content-Length: 47
    {
      "name" : "My other Team",
      "companyId" : 1
    }

#### Expected Responses

##### 200 OK

200 OK is returned with a JSON-object, describing the requested Team and its related User objects.

    {
      "status": "success",
      "data": {
        "id": 157,
        "companyId": 1,
        "active": true,
        "name": "My other Team",
        "users": []
      }
    }


## POST /team/user

Adds an existing User to an existing Team.

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature


#### Query-parameters

  * id|team|teamId (required)

###### id|team|teamId (required)

The identifier must be an integer.

#### Payload

A JSON User-object. Only the **id** property in the User-object is required.

    {
      "id" : 1
    }


#### Example request

    POST /team/user?team=157 HTTP/1.1
    Host: publicapi.tasklift.vvb
    Content-Type: application/json
    Accept: application/json
    Content-Length: 47
    {
      "id" : 1
    }

#### Expected Responses

##### 200 OK

200 OK is returned with a JSON-object, describing the updated Team and its related User objects.

    {
      "status": "success",
      "data": {
        "id": 157,
        "companyId": 1,
        "active": true,
        "name": "My other Team",
        "users": [
          {
            "id": 1,
            "legacyId": null,
            "firstName": "Anders",
            "lastName": "",
            "addresses": {},
            "emails": {},
            "phones": {},
            "status": true,
            "profile": null,
            "country": null
          },
          {
            "id": 2,
            "legacyId": null,
            "firstName": "Stefan",
            "lastName": "",
            "addresses": {},
            "emails": {},
            "phones": {},
            "status": true,
            "profile": null,
            "country": null
          }
        ]
      }
    }



## GET /user

Fetches a User JSON object, identified by the given user-ID.

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Query-parameters

  * id (required)

###### id (required)

The id is numeric.

#### Example request

    GET /user?id=2 HTTP/1.1
    Host: publicapi.tasklift.vvb
    Accept: application/json

#### Expected Responses

##### 200 OK

200 OK is returned with a JSON-object, describing the requested User. A legacyId is available, if the User has been migrated from the previous system version.

The Profile property is deprecated and should not be used.

    {
      "status": "success",
      "data": {
        "id": 1,
        "legacyId": null,
        "firstName": "Anders",
        "lastName": "",
        "addresses": {},
        "emails": {},
        "phones": {},
        "profile": null,
        "country": null
      }
    }

