# Public Template API


## <a name="get"></a>Get Template

GET /template

Request Template data for the specified Template

#### Required query parameters

##### id

    id=5bf6b61f63623910a306af01

Use the id query-parameter to specify the Template-ID.

#### Required headers

    Accept

Use the Accept header to determine the returned data format

#### Examples

Example request:

    > GET /template?id=5bf6b61f63623910a306af01 HTTP/1.1
    > Host: api.docupics.vvb
    > User-Agent: insomnia/6.2.3
    > Accept: application/xml
    > Api-Key: dev-andkrup
    > Time-Of-Request: 23 82 80 81 90 91
    > Signature: 0ae649b9af5eb115a5f0e56714ceaa9d00c59cbed672dc8ca68a7405c7855ead
    > Content-Length: 0

## <a name="create"></a>Create Template

POST /template


#### Examples
