[Public API](/api/public.html) |
[Device API](/api/device.html) |
[Admin API](/api/admin.html) |
[API Index](/api/index.html)

# Licenses (Public API)


Details about the Public API endpoints provided by the License service


### Return values

#### 200 OK

If the request succeeds, a Response with a success-status will be returned. If the request is of a type where an object is expected in return, the Response will be of the type DataResponse, and the expected object will be the value of the data property.

(MessageResponse)

    {
        "status": "success",
        "message": "<confirmation message>"
    }

(DataResponse)

    {
        "status": "success",
        "data": {...}
    }



#### 403 Forbidden
If the request does not authenticate

(MessageResponse)
    {
        "status": "error",
        "message": "unauthorised"
    }

## GET /license/search

List Licenses on the service

#### QueryString

Add the parameter on which to search. For example, in order to fetch a list of licenses for a company, use 'companyId':

    ?companyId=1

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    GET /license/search?companyId=1 HTTP/1.1
    Host: licenses.tasklift.vvb
    Content-Type: application/json
    Api-Key: dev-stefan
    Time-Of-Request: 12 21 21 32 32 32
    Signature: ed441b1ccedce9bced441b1ccedce9bced441b1ccedce9bced441b1ccedce9bc
    Accept: */*


## POST /license/create

Create a new License on the service


#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    POST /license/create HTTP/1.1
    Host: licenses.tasklift.vvb
    Content-Type: application/json
    Api-Key: dev-stefan
    Time-Of-Request: 12 21 21 32 32 32
    Signature: ed441b1ccedce9bced441b1ccedce9bced441b1ccedce9bced441b1ccedce9bc
    Accept: */*
    Content-Length: 213
    {
        "company" : {
            "id" : 1
        },
        "environment" : {
            "id" : 3
        },
        "user" : {
            "id" : 2
        }
    }



## PATCH /license/modify

Modify an existing License on the service.

Add the identifying **licenseId** as a querystring parameter.

This endpoint does not expect a full License object, you should only submit the properties that needs changing.


#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    PATCH /license/modify?licenseId=2 HTTP/1.1
    Host: licenses.tasklift.vvb
    Content-Type: application/json
    Api-Key: dev-stefan
    Time-Of-Request: 12 21 21 32 32 32
    Signature: ed441b1ccedce9bced441b1ccedce9bced441b1ccedce9bced441b1ccedce9bc
    Accept: */*
    Content-Length: 113
    {
        "userId" : {{ user.id  }}
    }


## POST /pairing/create

Create a new pairing

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    POST /pairing/create HTTP/1.1
    Host: licenses.tasklift.vvb
    Content-Type: application/json
    Api-Key: dev-stefan
    Time-Of-Request: 12 21 21 32 32 32
    Signature: ed441b1ccedce9bced441b1ccedce9bced441b1ccedce9bced441b1ccedce9bc
    Accept: */*
    Content-Length: 412
    {
        "license" : {
            "number" : "771F-8EE4-D2B5-9323-D5DC-2F2D-8424-A3AF"
        },
        "user" : {
            "id" : 651
        },
        "channel" : {
            "type" : "email",
            "identity" : "anders@tasklift.com"
        }
    }


## POST /client

Create a new ApiClient.

This endpoint will also issue a refresh-token that can be used as Api-Key value when requesting a new token to be issued.

#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    POST /client HTTP/1.1
    Host: licenses.tasklift.vvb
    Content-Type: application/json
    Api-Key: dev-stefan
    Time-Of-Request: 12 21 21 32 32 32
    Signature: ed441b1ccedce9bced441b1ccedce9bced441b1ccedce9bced441b1ccedce9bc
    Accept: */*
    Content-Length: 412
    {
        "handle" : "andkrup",
        "password" : "123456",
        "companyId" : 1,
        "context" : "admin"
    }


## POST /client/token

Issue a new Api-token.


#### Headers

  * Api-Key
  * Time-Of-Request
  * Signature

#### Example request

    POST /client/token HTTP/1.1
    Host: licenses.tasklift.vvb
    Content-Type: application/json
    Api-Key: dev-stefan
    Time-Of-Request: 12 21 21 32 32 32
    Signature: ed441b1ccedce9bced441b1ccedce9bced441b1ccedce9bced441b1ccedce9bc
    Accept: */*
    Content-Length: 412
    {
    	"handle" : "andkrup",
    	"password" : "123456",
    	"identity" : "admin",
    	"ttl" : null,
    	"allowed" : [
    		"127.0.0.1",
    		"192.168.56.1",
    		"192.168.56.113"
    	],
    	"denied" : []
    }

