var webpack = require('webpack');
var path = require('path');
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

var BUILD_DIR = path.resolve(__dirname, 'public/js');
var APPS_DIR = path.resolve(__dirname, 'apps');

var config = {
	entry: {
		admin :APPS_DIR + '/admin/client/admin.jsx',
		www :APPS_DIR + '/www/client/www.jsx'
	},
	target: 'web',
	output: {
		// path: path.join(BUILD_DIR, "js"),
		path: BUILD_DIR,
		filename: '[name].js',
		chunkFilename: "[id].chunk.js"
	},
	module : {
		loaders : [
			{
				test : /\.jsx?/,
				include : APPS_DIR,
				loader : 'babel-loader'
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			}
		]
	},
	plugins : [
		new CommonsChunkPlugin({
			filename: "application.js",
			name: "application"
		})
	]
};

module.exports = config;