<?php
/**
 * index.php.
 *
 * TODO: Documentation required!
 */

$loader = require_once __DIR__ . '/../vendor/autoload.php';
// Actually not necessary if the composer.json contains an autoload.psr-0 entry!
//$loader->add('SuperVillainHQ', __DIR__ . '/../apps/wiki/src');

$application = new \SuperVillainHQ\Wiki\WikiApplication(__DIR__ . '/../config/config.json');

$application->run();
